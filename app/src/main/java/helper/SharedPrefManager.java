package helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by system01 on 5/12/2017.
 */

public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "registration";

    private static final String KEY_USER_ID = "keyuserid";
    private static final String KEY_USER_NAME = "keyusername";
    private static final String KEY_USER_EMAIL = "keyuseremail";
    private static final String KEY_USER_MOBILE = "keyusermobile";
    private static final String KEY_USER_TYPE = "keyusertype";
    private static final String KEY_USER_CITY = "keyusercity";
    private static final String KEY_USER_STATE = "keyuserstate";
    private static final String KEY_USER_COUNTRY = "keyusercountry";
    private static final String KEY_USER_REGISTERDATE = "keyuserdate";
    private static final String KEY_USER_ISLOGIN = "keyuserlogin";


    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public boolean setUser(String KEY_USER_ID,
                           String KEY_USER_NAME,
                           String KEY_USER_EMAIL,
                           String KEY_USER_MOBILE,
                           String KEY_USER_TYPE,
                           String KEY_USER_CITY,
                           String KEY_USER_STATE,
                           String KEY_USER_COUNTRY,
                           String KEY_USER_REGISTERDATE) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(this.KEY_USER_ID, KEY_USER_ID);
        editor.putString(this.KEY_USER_NAME, KEY_USER_NAME);
        editor.putString(this.KEY_USER_EMAIL, KEY_USER_EMAIL);
        editor.putString(this.KEY_USER_MOBILE, KEY_USER_MOBILE);
        editor.putString(this.KEY_USER_TYPE, KEY_USER_TYPE);
        editor.putString(this.KEY_USER_CITY, KEY_USER_CITY);
        editor.putString(this.KEY_USER_STATE, KEY_USER_STATE);
        editor.putString(this.KEY_USER_COUNTRY, KEY_USER_COUNTRY);
        editor.putString(this.KEY_USER_REGISTERDATE, KEY_USER_REGISTERDATE);


        editor.apply();
        return true;
    }

 /*   public void setLoggedIn(boolean islogin) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(KEY_USER_ISLOGIN, islogin));
    }
    public void getLogin() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(KEY_USER_ISLOGIN))

    }

    public UserModel getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new UserModel(
                sharedPreferences.getInt(KEY_USER_ID, 0),
                sharedPreferences.getString(KEY_USER_NAME, null),
                sharedPreferences.getString(KEY_USER_EMAIL, null),
                sharedPreferences.getString(KEY_USER_GENDER, null)
        );
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }*/

}
