package distributor.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.Timer;
import java.util.TimerTask;

import changepassword.ChangePasswordActivity;
import distributor.completed.Distributor_Completed_Orders_Activity;
import distributor.neworders.Distributor_NewOrdersActivity;
import distributor.pending.Distributor_Pending_Orders_Activity;
import editprofile.EditProfileActivity;
import home.MainActivity;
import home.MyCustomPagerAdapter;

public class Distributor_Account_Activity extends AppCompatActivity {

    RelativeLayout pending_oreder;
    RelativeLayout completed_order;
    RelativeLayout editProfile;
    RelativeLayout changePassword;
    RelativeLayout logout_layout;



    private LinearLayout AccountLayout;

    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType, Accountname;
    private ViewPager myPager = null;
    private static int currentPage = 0;
    private static int NUM_PAGES = 3;
    private RelativeLayout new_order;
    private TextView AccountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor__account_);

        AccountName = (TextView) findViewById(R.id.account_name);

        new_order = (RelativeLayout) findViewById(R.id.new_orders_relativelayout);
        logout_layout = (RelativeLayout) findViewById(R.id.logout_layout);
        changePassword = (RelativeLayout) findViewById(R.id.change_password_layout);
        editProfile = (RelativeLayout) findViewById(R.id.edit_profile_relativelayout);
        completed_order = (RelativeLayout) findViewById(R.id.completed_order_relativelayout);
        pending_oreder = (RelativeLayout) findViewById(R.id.pending_order_relativelayout);



        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        UserType = myshare.getString("usertype", "").toString();
        Accountname = myshare.getString("name", "").toString();

        AccountName.setText("Hello "+Accountname);
        AccountName.setAllCaps(true);


        new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Account_Activity.this, Distributor_NewOrdersActivity.class));
            }
        });

        pending_oreder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Account_Activity.this, Distributor_Pending_Orders_Activity.class));
            }
        });

        completed_order.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Account_Activity.this, Distributor_Completed_Orders_Activity.class));
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Account_Activity.this, EditProfileActivity.class));
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Account_Activity.this, ChangePasswordActivity.class));
            }
        });

        logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Distributor_Account_Activity.this);
                alertDialog.setMessage("Logout successfully !");
                alertDialog.setTitle("WABCO");
                alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        startActivity(new Intent(Distributor_Account_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        edit.putBoolean("islogin", false).commit();

                    }
                });
                alertDialog.show();
            }
        });




        MyCustomPagerAdapter adapter = new MyCustomPagerAdapter(Distributor_Account_Activity.this);
        myPager = (ViewPager) findViewById(R.id.viewpager);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setViewPager(myPager);
        indicator.setRadius(5 * density);


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                myPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }
}
