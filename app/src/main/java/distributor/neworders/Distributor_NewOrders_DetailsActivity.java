package distributor.neworders;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;


public class Distributor_NewOrders_DetailsActivity extends Activity {

    private ImageView Backbtn;
    private View heade_Layout;
    private TextView Tittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor__new_orders__details);

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Tittle.setText("New Orders Details");

        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
