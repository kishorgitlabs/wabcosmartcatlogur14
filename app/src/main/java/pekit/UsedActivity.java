package pekit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class UsedActivity extends Activity {


    private ImageView vehicleBackImageView;

    public List<String> descriptionList;


    public ProgressDialog loadDialog;
    private ListView partlist;
    private String partnunmber;
    private Alertbox box = new Alertbox(UsedActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private ImageView Cart_Icon;
    private String UserType;
    private ListView pelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_used);
        vehicleBackImageView = (ImageView) findViewById(R.id.back);



        partnunmber = getIntent().getStringExtra("PartCode");
        TextView head = (TextView) findViewById(R.id.head_textView1);
        head.setText(partnunmber);
        Log.v("Part no frm intent ", partnunmber);
        head.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();

            }
        });




        partlist = (ListView) findViewById(R.id.listView);
        new RetrievePartItemAsyn().execute(new String[]{partnunmber});


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(UsedActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(UsedActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(UsedActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(UsedActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(UsedActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(UsedActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(UsedActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(UsedActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(UsedActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(UsedActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(UsedActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(UsedActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(String... position) {
            PEkitDAO connection = new PEkitDAO(UsedActivity.this);
            PEkitDTO pekitDTO = new PEkitDTO();
            pekitDTO = connection.retrieveUSed(position[0]);
            descriptionList = pekitDTO.getUsedAsspList();
            if (descriptionList.isEmpty()) {
                return "Empty";
            }

            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {

                box.showAlertbox("Used Assembly are not Available !");


            } else {
                partlist.setAdapter(new Used_Adapter(UsedActivity.this, descriptionList));
            }


        }
    }


    public void onBackPressed() {
        super.onBackPressed();
    }


}