package pekit;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

public class Used_Adapter extends ArrayAdapter<String> {

	private Context context;
	List<String> partnoList;
	public Used_Adapter(Context context, List<String> descriptionList) 
	{
		 super(context, R.layout.usedsimple_list_item, descriptionList);
		 this.partnoList = descriptionList;
		 this.context=context;
		 
		// TODO Auto-generated constructor stub
	}
	 public View getView(final int position, View convertView, ViewGroup parent) 
	 {

		 convertView = null;
		 
	        if (convertView == null)
	        {
	        	
	        	convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.usedsimple_list_item, null);
		           
	        	TextView snotxt = (TextView) convertView.findViewById(R.id.usedtxt);
	        	 snotxt.setText(partnoList.get(position));
	        	 
	        	 snotxt.setOnClickListener(new OnClickListener() {
	 				
	 				private String partno;

					@Override
	 				public void onClick(View v) 
	 				{
	 					// TODO Auto-generated method stub
	 					
	 					
	 					
	 					Intent vehiclepartnotxtFamily = new Intent(context, UsedPartItemActivity.class);
	 					vehiclepartnotxtFamily.putExtra("part",partnoList.get(position));
	 					context.startActivity(vehiclepartnotxtFamily);
	 					
	 				}
	 			});
	 	        
	           
	        } 
	       
	      
	       
	        return convertView;
	    }
	}

