package login;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.dealer.pending.responce.CommenResult;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import serviceengineer.account.FieldEngineer_Account;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class UserLoginActivity extends AppCompatActivity {


  private static final String TAG_SELECT_COLOR_DIALOG = "TAG_SELECT_COLOR_DIALOG";
  private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
      .addConverterFactory(GsonConverterFactory.create());
  public static Retrofit retrofit = builder.build();

  private Button createlogin, login;
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private String UserType;
  private TextView forgotPassword;
  private EditText UserName, Password;
  private Alertbox box = new Alertbox(UserLoginActivity.this);
  private String SelectedState, SelectedUser;
  private String namestring, phonestring, emailstring, citystring, countrystring, currentDate,
      android_id;
  private String output = "", UserID = "";
  private Intent fromIntent;
  private String EmailId;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user_login);


    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();
    UserType = myshare.getString("usertype", "").toString();

    UserName = (EditText) findViewById(R.id.username);
    Password = (EditText) findViewById(R.id.password);

    login = (Button) findViewById(R.id.login);
    createlogin = (Button) findViewById(R.id.createlogin);

    forgotPassword = (TextView) findViewById(R.id.forgotpassword);
    fromIntent = getIntent();
    forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    UserType = myshare.getString("usertype", "").toString();
    if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC"))) {
      if (myshare.getBoolean("islogincreated", false))
        createlogin.setVisibility(View.GONE);
      else
        createlogin.setVisibility(View.VISIBLE);
    } else
      createlogin.setVisibility(View.GONE);

    /*
     * if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")) &&
     * myshare.getString("password","").equals("")) { createlogin.setVisibility(View.VISIBLE); }
     * else { createlogin.setVisibility(View.GONE); }
     */

    login.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (UserName.getText().toString().equals("")) {
          StyleableToast st =
              new StyleableToast(UserLoginActivity.this, "Enter user name !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (Password.getText().toString().equals("")) {
          StyleableToast st =
              new StyleableToast(UserLoginActivity.this, "Enter password !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();

        } else {
          CheckInternet();
        }
      }
    });


    createlogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(UserLoginActivity.this, CreateLoginActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
      }
    });

    forgotPassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String[] mailId = new String[1];

        new MaterialDialog.Builder(UserLoginActivity.this).title("Forgot Password")
            .content("Your password will be sent to your registered email account.")
            .autoDismiss(false)
            .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
            .input("Your registered email id", "", new MaterialDialog.InputCallback() {
              @Override
              public void onInput(MaterialDialog dialog, CharSequence input) {
                // Do something
                EmailId = input.toString();
              }
            }).positiveText("Send").negativeText("Cancel")
            .onPositive(new MaterialDialog.SingleButtonCallback() {
              @Override
              public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if (dialog.getInputEditText().length() != 0) {

                  if (!isValidEmail(dialog.getInputEditText().getText().toString())) {
                    StyleableToast st = new StyleableToast(UserLoginActivity.this,
                            "Enter valid email id !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                  } else {
                    dialog.dismiss();
                    ForgotPassword(dialog.getInputEditText().getText().toString());
                  }



                } else {
                  StyleableToast st = new StyleableToast(UserLoginActivity.this,
                          "Enter registered email id !", Toast.LENGTH_SHORT);
                  st.setBackgroundColor(getResources().getColor(R.color.red));
                  st.setTextColor(Color.WHITE);
                  st.setMaxAlpha();
                  st.show();
                }


              }
            }).onNegative(new MaterialDialog.SingleButtonCallback() {
              @Override
              public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
              }
            }).show();
    }
      });
      final ImageView menu = (ImageView) findViewById(R.id.menu);
      menu.setOnClickListener(new View.OnClickListener() {

          public void onClick(View v) {
              @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(UserLoginActivity.this, R.style.PopupMenu);
              final PopupMenu pop = new PopupMenu(wrapper, v);
              pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                  @Override
                  public void onDismiss(PopupMenu arg0) {
                      // TODO Auto-generated method stub
                      pop.dismiss();
                  }
              });
              pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                  public boolean onMenuItemClick(MenuItem item) {
                      switch (item.getItemId()) {
                          case R.id.search:
                              startActivity(new Intent(UserLoginActivity.this, SearchActivity.class));
                              break;
                          case R.id.notification:
                              startActivity(new Intent(UserLoginActivity.this, NotificationActivity.class));
                              break;
                          case R.id.vehicle:
                              startActivity(new Intent(UserLoginActivity.this, VehicleMakeActivity.class));
                              break;
                          case R.id.product:
                              startActivity(new Intent(UserLoginActivity.this, ProductFamilyActivity.class));
                              break;
                          case R.id.performance:
                              startActivity(new Intent(UserLoginActivity.this, PE_Kit_Activity.class));
                              break;
                          case R.id.contact:
                              startActivity(new Intent(UserLoginActivity.this, Network_Activity.class));
                              break;
                          case R.id.askwabco:
                              startActivity(new Intent(UserLoginActivity.this, AskWabcoActivity.class));
                              break;
                          case R.id.pricelist:
                              startActivity(new Intent(UserLoginActivity.this, PriceListActivity.class));
                              break;
                          case R.id.update:
                              WabcoUpdate update = new WabcoUpdate(UserLoginActivity.this);
                              update.checkVersion();
                              break;
                      }
                      return false;
                  }
              });
              pop.inflate(R.menu.main);
              pop.show();
          }
      });
  }

  public final static boolean isValidEmail(CharSequence target) {
    if (target == null) {
      return false;
    } else {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
  }

  private void ForgotPassword(String mailID) {

    NetworkConnection isnet = new NetworkConnection(UserLoginActivity.this);
    if (isnet.CheckInternet()) {
      SendEmailToForgotPassword(mailID);
    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }

  }

  @Override
  protected void onResume() {
    super.onResume();
    UserType = myshare.getString("usertype", "").toString();
    if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC"))) {
      if (myshare.getBoolean("islogincreated", false))
        createlogin.setVisibility(View.GONE);
      else
        createlogin.setVisibility(View.VISIBLE);
    } else
      createlogin.setVisibility(View.GONE);

  }

  private void CheckInternet() {

    NetworkConnection isnet = new NetworkConnection(UserLoginActivity.this);
    if (isnet.CheckInternet()) {
      CheckUserLogin();

    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }


  }


  private void CheckUserLogin() {
    final ProgressDialog loading =
        ProgressDialog.show(this, "Login", "Please wait...", false, false);

    namestring = myshare.getString("name", "").toString();
    phonestring = myshare.getString("phone", "").toString();
    emailstring = myshare.getString("email", "").toString();
    citystring = myshare.getString("city", "").toString();
    SelectedState = myshare.getString("state", "").toString();
    UserID = myshare.getString("id", "").toString();
    currentDate = myshare.getString("registereddate", "").toString();
    android_id = myshare.getString("mobileid", "").toString();
    countrystring = myshare.getString("country", "").toString();
    UserType = myshare.getString("usertype", "").toString();

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();

    // Creating object for our interface
    APIService api = retrofit.create(APIService.class);
    Call<CommenResult> user;
    if (UserType.equals("Wabco employee")) {
      user = api.CheckUserLoginWabcoempoyee(UserID, UserName.getText().toString(),
          Password.getText().toString(), emailstring);
    } else {
      user = api.CheckUserLogin(UserID, UserName.getText().toString(),
          Password.getText().toString(), UserType);
    }

    user.enqueue(new Callback<CommenResult>() {
      @Override
      public void onResponse(Call<CommenResult> list, Response<CommenResult> response) {
        if (response.isSuccessful()) {
          CommenResult user = response.body();
          loading.dismiss();
          Log.v("Result", response.body().getResult());
          if (response.body().getResult().equals("Success") || response.body().getResult().equals("Valid User")) {
            SuccessLogin(user);
          } else if (response.body().getResult().equals("NotSuccess")) {
            FailedLogin();
          } else {
            box.showAlertbox(getResources().getString(R.string.server_error));
          }
        } else {
          loading.dismiss();
          box.showAlertbox(getResources().getString(R.string.server_error));
        }
      }

      @Override
      public void onFailure(Call<CommenResult> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.slow_internet_connection));
      }
    });

  }

  private void FailedLogin() {

    box.showAlertbox("Invalid user !");
  }

  private void SuccessLogin(final CommenResult user) {

    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserLoginActivity.this);
    alertDialog.setMessage("Logged in successfully !");
    alertDialog.setTitle("WABCO");
    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // TODO Auto-generated method stub
        edit.putBoolean("islogin", true).commit();
        dialog.dismiss();

        if (getIntent().getStringExtra("from").equals("home")
            || getIntent().getStringExtra("from").equals("editprofile")
            || getIntent().getStringExtra("from").equals("notification")) {
          if (UserType.equals("Wabco employee")) {
              edit.putString("id", user.getData().toString()).commit();
              startActivity(new Intent(UserLoginActivity.this, FieldEngineer_Account.class));
            finish();
          }
          else
            startActivity(new Intent(UserLoginActivity.this, Dealer_Account_Activity.class));
          finish();
        } else {
          onBackPressed();
        }
      }
    });
    alertDialog.show();

  }

  private void SendEmailToForgotPassword(String mailID) {

    final ProgressDialog loading =
        ProgressDialog.show(this, "Sending", "Please wait...", false, false);

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();

    // Creating object for our interface
    APIService api = retrofit.create(APIService.class);
    Call<CommenResult> forgot = api.ForgotPassword(myshare.getString("id", ""), mailID);

    forgot.enqueue(new Callback<CommenResult>() {
      @Override
      public void onResponse(Call<CommenResult> call, Response<CommenResult> response) {
        loading.dismiss();
        if (response.body().getResult().equals("Success")) {
          box.showAlertbox("Password has been sent to your email id !");
        } else if (response.body().getResult().equals("MailIdNotFound")) {
          box.showAlertbox("Invalid email id. Please check your email id !");
        } else {
          box.showAlertbox(getResources().getString(R.string.server_error));
        }
      }


      @Override
      public void onFailure(Call<CommenResult> call, Throwable t) {
        loading.dismiss();
        box.showAlertbox(getResources().getString(R.string.nointernetmsg));
      }
    });


  }


}
