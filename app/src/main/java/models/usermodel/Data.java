
package models.usermodel;

import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class Data {

    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("deletestatus")
    private String mDeletestatus;
    @SerializedName("deviceid")
    private String mDeviceid;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("state")
    private String mState;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("usertype")
    private String mUsertype;
    @SerializedName("Pincode")
    private String mPincode;

//public ASCData(ASCData other) {
//        this.mCity = other.mCity;
//        this.mCountry = other.mCountry;
//        this.mDate = other.mDate;
//        this.mDeletestatus = other.mDeletestatus;
//        this.mDeviceid = other.mDeviceid;
//        this.mEmail = other.mEmail;
//        this.mId = other.mId;
//        this.mName = other.mName;
//        this.mPassword = other.mPassword;
//        this.mPhone = other.mPhone;
//        this.mState = other.mState;
//        this.mUsername = other.mUsername;
//        this.mUsertype = other.mUsertype;
//    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeletestatus() {
        return mDeletestatus;
    }

    public void setDeletestatus(String deletestatus) {
        mDeletestatus = deletestatus;
    }

    public String getDeviceid() {
        return mDeviceid;
    }

    public void setDeviceid(String deviceid) {
        mDeviceid = deviceid;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }
}
