package models.distributor;

/**
 * Created by system01 on 5/12/2017.
 */

public class Distributor_history_Model {

    private String orderNumber;
    private String deliveryAddress;
    private String deliveryDate;

    public Distributor_history_Model(String orderNumber, String deliveryAddress, String deliveryDate)
    {
        this.orderNumber = orderNumber;
        this.deliveryAddress = deliveryAddress;
        this.deliveryDate = deliveryDate;
    }


    public String getOrderNumber()
    {
        return orderNumber;
    }

    public String getDeliveryAddress()
    {
        return deliveryAddress;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }
}
