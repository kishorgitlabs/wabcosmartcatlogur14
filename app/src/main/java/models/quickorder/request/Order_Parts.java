package models.quickorder.request;

/**
 * Created by system01 on 6/19/2017.
 */

public class Order_Parts {
    private String id;

    private String DeliveryDate;

    private String Quantity;

    private String OrderNumber;

    private String DeliveryStatus;

    private String PartPrice;

    private String PartName;

    private String TotalAmount;

    private String PartNumber;
    private String Description;
    private String Active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String DeliveryDate) {
        this.DeliveryDate = DeliveryDate;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        this.OrderNumber = OrderNumber;
    }

    public String getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus) {
        this.DeliveryStatus = DeliveryStatus;
    }

    public String getPartPrice() {
        return PartPrice;
    }

    public void setPartPrice(String PartPrice) {
        this.PartPrice = PartPrice;
    }

    public String getPartName() {
        return PartName;
    }

    public void setPartName(String PartName) {
        this.PartName = PartName;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    public String getPartNumber() {
        return PartNumber;
    }

    public void setPartNumber(String PartNumber) {
        this.PartNumber = PartNumber;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", DeliveryDate = " + DeliveryDate + ", Quantity = " + Quantity + ", OrderNumber = " + OrderNumber + ", DeliveryStatus = " + DeliveryStatus + ", PartPrice = " + PartPrice + ", PartName = " + PartName + ", TotalAmount = " + TotalAmount + ", PartNumber = " + PartNumber + "]";
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }
}
