
package models.dealer.pending.responce;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class OrderDetails {

    @SerializedName("data")
    private List<OrderDetailsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<OrderDetailsResult> getData() {
        return mData;
    }

    public void setData(List<OrderDetailsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
