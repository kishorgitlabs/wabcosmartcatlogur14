
package models.asc;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ASCDetails {

    @SerializedName("data")
    private ASCData mData;
    @SerializedName("result")
    private String mResult;

    public ASCData getData() {
        return mData;
    }

    public void setData(ASCData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
