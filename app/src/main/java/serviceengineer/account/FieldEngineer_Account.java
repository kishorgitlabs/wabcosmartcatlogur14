package serviceengineer.account;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.Timer;
import java.util.TimerTask;

import askwabco.AskWabcoActivity;
import changepassword.ChangePasswordActivity;
import directory.WabcoUpdate;
import editprofile.EditProfileActivity;
import home.MainActivity;
import home.MyCustomPagerAdapter;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import serviceengineer.completed.FieldEngineer_Completed_Orders_Activity;
import serviceengineer.pending.FieldEngineer_Pending_Orders_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class FieldEngineer_Account extends Activity {
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private TextView Account_name;
    private ViewPager myPager = null;
    private static int currentPage = 0;
    private static int NUM_PAGES = 3;
    private RelativeLayout Pending_orders_relativelayout,Completed_order_relativelayout,edit_Profile_relativelayout,change_Password_layout,continue_Oreder,Logout_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_field_engineer__account);

        Account_name =(TextView) findViewById(R.id.account_name);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();



        Account_name.setText("Hello "+myshare.getString("name",""));
        MyCustomPagerAdapter adapter = new MyCustomPagerAdapter(getApplicationContext());
        myPager = (ViewPager) findViewById(R.id.viewpager);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        Pending_orders_relativelayout = (RelativeLayout) findViewById(R.id.pending_orders_relativelayout);
        Completed_order_relativelayout = (RelativeLayout) findViewById(R.id.completed_order_relativelayout);
        edit_Profile_relativelayout = (RelativeLayout) findViewById(R.id.edit_profile_relativelayout);
        change_Password_layout = (RelativeLayout) findViewById(R.id.change_password_layout);
        Logout_layout = (RelativeLayout) findViewById(R.id.logout_layout);
        continue_Oreder = (RelativeLayout) findViewById(R.id.continue_order);



        //ShowSelection();
        continue_Oreder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FieldEngineer_Account.this,MainActivity.class));
            }
        });


        Pending_orders_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FieldEngineer_Account.this, FieldEngineer_Pending_Orders_Activity.class));

            }
        });

        Completed_order_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FieldEngineer_Account.this, FieldEngineer_Completed_Orders_Activity.class));

            }
        });



        edit_Profile_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FieldEngineer_Account.this, EditProfileActivity.class).putExtra("from","account"));

            }
        });


        change_Password_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FieldEngineer_Account.this, ChangePasswordActivity.class));

            }
        });


        Logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(FieldEngineer_Account.this);
                alertDialog.setMessage("Logout successfully !");
                alertDialog.setTitle("WABCO");
                alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        startActivity(new Intent(FieldEngineer_Account.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        edit.putBoolean("islogin", false).commit();


                    }
                });
                alertDialog.show();


            }
        });


        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setViewPager(myPager);
        indicator.setRadius(5 * density);


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                myPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(FieldEngineer_Account.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(FieldEngineer_Account.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(FieldEngineer_Account.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(FieldEngineer_Account.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(FieldEngineer_Account.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(FieldEngineer_Account.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(FieldEngineer_Account.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(FieldEngineer_Account.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(FieldEngineer_Account.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(FieldEngineer_Account.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(FieldEngineer_Account.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }



}


