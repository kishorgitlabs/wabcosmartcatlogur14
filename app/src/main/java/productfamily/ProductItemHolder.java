package productfamily;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/* compiled from: ProductFamilyItemAdapter */
class ProductItemHolder {
    TextView description;
    TextView manufacture;
    TextView partno;
    ImageView product;
    ImageView repair;
    ImageView service;
    TextView sno;
    Button addcart;
    ProductItemHolder() {
    }
}
