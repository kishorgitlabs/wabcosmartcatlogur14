package productfamily;

import java.util.List;

public class ProductFamilyDTO {
    private List<String> Flag_List;
    private List<String> description_List;
    private List<String> productIDList;
    private List<String> productNameList;
    private List<String> productPartno_List;
    private List<String> repair_List;
    private List<String> serice_List;
    private List<String> vehicleIDList;
    private List<String> vehicleNameList;
    private List<String> vehicleNameList2;

    public List<String> getVehicleIDList() {
        return this.vehicleIDList;
    }

    public void setVehicleIDList(List<String> vehicleIDList) {
        this.vehicleIDList = vehicleIDList;
    }

    public List<String> getProductNameList() {
        return this.productNameList;
    }

    public void setProductNameList(List<String> productNameList) {
        this.productNameList = productNameList;
    }

    public List<String> getProductIDList() {
        return this.productIDList;
    }

    public void setProductIDList(List<String> productIDList) {
        this.productIDList = productIDList;
    }

    public void setVehicleNameList(List<String> vehicleNameList) {
        this.vehicleNameList = vehicleNameList;
    }

    public List<String> getVehicleName() {
        return this.vehicleNameList;
    }

    public void setPart_No_List(List<String> productPartno_List) {
        this.productPartno_List = productPartno_List;
    }

    public void setDescription(List<String> description_List) {
        this.description_List = description_List;
    }

    public void setService_Part(List<String> serice_List) {
        this.serice_List = serice_List;
    }

    public void setRepair_No(List<String> repair_List) {
        this.repair_List = repair_List;
    }

    public List<String> getDescription() {
        return this.description_List;
    }

    public List<String> getPartno_List() {
        return this.productPartno_List;
    }

    public List<String> getService_List() {
        return this.serice_List;
    }

    public List<String> getRepair_List() {
        return this.repair_List;
    }

    public void setVehicleForSpinnerNameList(List<String> vehicleNameList2) {
        this.vehicleNameList2 = vehicleNameList2;
    }

    public List<String> getVehicleNameListForSpinner() {
        return this.vehicleNameList2;
    }

    public void setFlag_List(List<String> flag_List) {
    
        this.Flag_List = flag_List;
    }

    public List<String> getFlag_List() {
        return this.Flag_List;
    }
}
