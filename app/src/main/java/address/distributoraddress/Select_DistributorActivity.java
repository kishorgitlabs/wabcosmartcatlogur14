package address.distributoraddress;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import adapter.Select_Distributor_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import addcart.Order_ConfirmationActivity;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import api.ErrorUtils;
import askwabco.AskWabcoActivity;
import contact.ContactDAO;
import contact.ContactDTO;
import directory.WabcoUpdate;
import home.MainActivity;
import models.distributor.DistributorData;
import models.distributor.DistributorModel;
import models.errormodel.APIError;
import network.NetworkConnection;
import notification.NotificationActivity;
import okhttp3.ResponseBody;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class Select_DistributorActivity extends AppCompatActivity {

  private Button placeOrder, Dist_favorites;
  private SharedPreferences distributor_share;
  private SharedPreferences.Editor distributor_edit;
  private String[] drawerResourcesList;
  private List<String> stateList;
  private MaterialSpinner state, city;
  private List<String> cityList;
  private RecyclerView recycler_view;
  private Alertbox box = new Alertbox(Select_DistributorActivity.this);

  private ProgressDialog loading;
  private String SelectedState, SelectedCity = "";
  private String comingFrom;
  private List<DistributorData> distributorModel;
  private Select_Distributor_Adapter adapter =
      new Select_Distributor_Adapter(Select_DistributorActivity.this, distributorModel);

  private SharedPreferences dealershare;
  private SharedPreferences.Editor dealeredit;
  private ImageView back;
    private ImageView Cart_Icon;

    @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select__distributor);

    placeOrder = (Button) findViewById(R.id.placeorder);
    Dist_favorites = (Button) findViewById(R.id.favorites_dist);
    Cart_Icon = (ImageView) findViewById(R.id.cart_icon);

    state = (MaterialSpinner) findViewById(R.id.statespinner);
    city = (MaterialSpinner) findViewById(R.id.cityspinner);

    state.setBackgroundResource(R.drawable.spinner_back_green);
    city.setBackgroundResource(R.drawable.spinner_back_green);

    recycler_view = (RecyclerView) findViewById(R.id.listView);
    recycler_view.setHasFixedSize(true);
    recycler_view.setHasFixedSize(true);
    LinearLayoutManager llm = new LinearLayoutManager(this);
    llm.setOrientation(LinearLayoutManager.VERTICAL);
    recycler_view.setLayoutManager(llm);

    stateList = new ArrayList<String>();
    cityList = new ArrayList<String>();

    distributor_share = getSharedPreferences("distributor_address", MODE_PRIVATE);
    distributor_edit = distributor_share.edit();
    distributor_edit.clear();
    distributor_edit.commit();

    dealershare = getSharedPreferences("registration", MODE_PRIVATE);
    dealeredit = dealershare.edit();

    comingFrom = getIntent().getStringExtra("from");


    placeOrder.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {


        if (!distributor_share.getString("distributor_name", "").equals("")) {

          SaveToFavoriteDistributor();

          startActivity(
              new Intent(Select_DistributorActivity.this, Order_ConfirmationActivity.class)
                  .putExtra("from", comingFrom));

        } else {
          StyleableToast st = new StyleableToast(Select_DistributorActivity.this,
              getResources().getString(R.string.select_distributor), Toast.LENGTH_SHORT);
          st.setBackgroundColor(
              Select_DistributorActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
      }
    });

    Dist_favorites.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(
            new Intent(Select_DistributorActivity.this, Favorite_DistributorActivity.class)
                .putExtra("for", "select").putExtra("from",comingFrom));
      }
    });

    state.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        SelectedState = item.toString();
        if (!SelectedState.equals(getResources().getString(R.string.city_text))) {
          new RetriveCitiesForSpinner().execute();
        }

        // GetDistributor(SelectedState, SelectedCity);
      }
    });

    city.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        SelectedCity = item.toString();
        if (!SelectedCity.equals(getResources().getString(R.string.city_text))) {
          String query = "select distinct * from address where  City ='" + SelectedCity
              + "' and State_Name='" + SelectedState + "' and contact_type ='Distributor' ";
          new RetriveDealerAddress().execute(query);
        } else {
          StyleableToast st = new StyleableToast(Select_DistributorActivity.this, "Select city !",
              Toast.LENGTH_SHORT);
          st.setBackgroundColor(
              Select_DistributorActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }

      }
    });


    this.back = (ImageView) findViewById(R.id.back);
    this.back.setOnClickListener(new View.OnClickListener() {


      public void onClick(View arg0) {
        onBackPressed();
      }
    });
    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(Select_DistributorActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Select_DistributorActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Select_DistributorActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Select_DistributorActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Select_DistributorActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Select_DistributorActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Select_DistributorActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Select_DistributorActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Select_DistributorActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Select_DistributorActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Select_DistributorActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });

      Cart_Icon.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v)
          {
              CartDAO cartDAO = new CartDAO(Select_DistributorActivity.this);
              CartDTO cartDTO = cartDAO.GetCartItems();
              if(cartDTO.getPartCodeList() == null)
              {
                  StyleableToast st =
                          new StyleableToast(Select_DistributorActivity.this, "Cart is Empty !", Toast.LENGTH_SHORT);
                  st.setBackgroundColor(Select_DistributorActivity.this.getResources().getColor(R.color.red));
                  st.setTextColor(Color.WHITE);
                  st.setMaxAlpha();
                  st.show();
              }else
              {
                  startActivity(new Intent(Select_DistributorActivity.this, Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
              }
              //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
          }
      });


    // CheckInternet();
    LoadState();


  }

  private void LoadState() {
    new RetriveStatesForSpinner().execute();
  }


  private void CheckInternet() {

    if (new NetworkConnection(this).CheckInternet()) {
      GetDistributorStateAndCity();
    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }
  }

  private void GetDistributor(String state, String city) {

    loading = ProgressDialog.show(Select_DistributorActivity.this, "Loading", "Please wait...",
        false, false);

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();

    APIService api = retrofit.create(APIService.class);

    Call<DistributorModel> distributor = api.getDistributors(state, city);
    distributor.enqueue(new Callback<DistributorModel>() {
      @Override
      public void onResponse(Call<DistributorModel> distributorList,
          Response<DistributorModel> response) {

        loading.dismiss();

        if (response.isSuccessful()) {
          distributorModel = response.body().getData();
          LoadDistributors(distributorModel);
          // LoadCity(distributorModel);
        } else {
          loading.dismiss();
          // handle request errors yourself
          ResponseBody errorBody = response.errorBody();
          APIError apiError = ErrorUtils.parseError(response);

          StyleableToast st = new StyleableToast(Select_DistributorActivity.this,
              apiError.message(), Toast.LENGTH_SHORT);
          st.setBackgroundColor(
              Select_DistributorActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
      }

      @Override
      public void onFailure(Call<DistributorModel> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.server_error));
      }
    });
  }

  private void GetDistributorStateAndCity() {

    loading = ProgressDialog.show(Select_DistributorActivity.this, "Loading", "Please wait...",
        false, false);

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();

    APIService api = retrofit.create(APIService.class);

    Call<DistributorModel> distributor = api.getALLDistributors();
    distributor.enqueue(new Callback<DistributorModel>() {
      @Override
      public void onResponse(Call<DistributorModel> distributorList,
          Response<DistributorModel> response) {

        loading.dismiss();

        if (response.isSuccessful()) {

          distributorModel = response.body().getData();
          LoadDistributorsStateCities(distributorModel);
          LoadDistributors(distributorModel);
        } else {
          loading.dismiss();
          // handle request errors yourself
          ResponseBody errorBody = response.errorBody();
          APIError apiError = ErrorUtils.parseError(response);

          StyleableToast st = new StyleableToast(Select_DistributorActivity.this,
              apiError.message(), Toast.LENGTH_SHORT);
          st.setBackgroundColor(
              Select_DistributorActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
      }

      @Override
      public void onFailure(Call<DistributorModel> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.server_error));
      }
    });
  }

  private void LoadDistributorsStateCities(List<DistributorData> distributorData) {
    Set<String> setWithUniqueState = null, setWithUniqueCity = null;
    for (int i = 0; i < distributorData.size(); i++) {
      stateList.add(distributorData.get(i).getState());
      cityList.add(distributorData.get(i).getCity());

      setWithUniqueState = new HashSet<>(stateList);
      setWithUniqueCity = new HashSet<>(cityList);
    }
    if (stateList.size() != 0) {
      stateList.clear();
      cityList.clear();

      stateList = new ArrayList<>(setWithUniqueState);
      cityList = new ArrayList<>(setWithUniqueCity);

      state.setItems(stateList);
      city.setItems(cityList);
    }

    state.setPadding(30, 0, 0, 0);
    city.setPadding(30, 0, 0, 0);
  }

  private void LoadCity(List<DistributorData> distributorModel) {
    Set<String> setWithUniqueCity = null;
    for (int i = 0; i < distributorModel.size(); i++) {
      cityList.add(distributorModel.get(i).getCity());
      setWithUniqueCity = new HashSet<>(cityList);
    }
    cityList.clear();
    cityList = new ArrayList<>(setWithUniqueCity);
    city.setItems(cityList);

  }

  class RetriveStatesForSpinner extends AsyncTask<Void, Void, String> {

    protected String doInBackground(Void... params) {
      ContactDAO contactDAO = new ContactDAO(Select_DistributorActivity.this);
      ContactDTO contactDTO = new ContactDTO();
      contactDTO = contactDAO.retriveState("Distributor");
      stateList = contactDTO.getStateName();
      if (stateList.isEmpty()) {
        return "unsuccess";
      }
      return "success";
    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (result.equals("success")) {
        stateList.add(0, getResources().getString(R.string.state_text));
        state.setItems(stateList);
        return;
      }

    }
  }

  class RetriveCitiesForSpinner extends AsyncTask<Void, Void, String> {

    protected String doInBackground(Void... params) {
      ContactDAO contactDAO = new ContactDAO(Select_DistributorActivity.this);
      ContactDTO contactDTO = new ContactDTO();
      contactDTO = contactDAO.retriveCity(SelectedState, "Distributor");
      cityList = contactDTO.getCityName();

      if (cityList.isEmpty()) {
        return "unsuccess";
      }
      return "success";
    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (result.equals("success")) {
        cityList.add(0, getResources().getString(R.string.city_text));
        city.setItems(cityList);
      }

    }
  }


  class RetriveDealerAddress extends AsyncTask<String, Void, String> {


    protected void onPreExecute() {
      super.onPreExecute();
      loading = ProgressDialog.show(Select_DistributorActivity.this, "Loading", "Please wait...",
          false, false);


    }

    protected String doInBackground(String... params) {
      ContactDAO contactDAO = new ContactDAO(Select_DistributorActivity.this);
      ContactDTO contactDTO = new ContactDTO();
      Log.v("Select_DistributorActivity  ", params[0]);
      contactDTO = contactDAO.retriveAddress(params[0]);
      distributorModel = new ArrayList<DistributorData>();
      for (int i = 0; i < contactDTO.getStateName().size(); i++) {

        DistributorData distributorData = new DistributorData();
        distributorData.setId(contactDTO.getDistributor_code().get(i));
        distributorData.setName(contactDTO.getName().get(i));
        distributorData.setState(contactDTO.getStateName().get(i));
        distributorData.setCity(contactDTO.getCityName().get(i));
        distributorData.setAddress(contactDTO.getAddress().get(i));
        distributorData.setMobileNumber(contactDTO.getphone().get(i));
        distributorData.setEmail(contactDTO.getEmail1().get(i));
        distributorData.setAddedfavorite(CheckIsAlreadyAdded(contactDTO.getAddress().get(i)));
        distributorModel.add(distributorData);

      }
      contactDAO.closeDatabase();
      return "success";
    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      loading.dismiss();
      LoadDistributors(distributorModel);

    }

  }

  private Boolean CheckIsAlreadyAdded(String address) {

    DBHelper dbHelper = new DBHelper(Select_DistributorActivity.this);
    SQLiteDatabase db = dbHelper.readDataBase();
    String query="select * from FavoriteDistributor where address = '"+address+"'";
    Cursor cursor = db.rawQuery(query, null);

    if (cursor.moveToFirst()) {
      do {
       return true;
      } while (cursor.moveToNext());
    }
    else return false;

  }

  private void LoadDistributors(List<DistributorData> distributorModel) {
    adapter = new Select_Distributor_Adapter(Select_DistributorActivity.this, distributorModel);
    recycler_view.setAdapter(adapter);
  }

  private void SaveToFavoriteDistributor() {
    CartDAO cartDAO = new CartDAO(Select_DistributorActivity.this);



    for (int i = 0; i < adapter.getItemCount(); i++) {
      if (distributorModel.get(i).getFavorite()) {
        cartDAO.addToFavoriteDistributor(distributorModel, i);
      }
    }

  }
}
