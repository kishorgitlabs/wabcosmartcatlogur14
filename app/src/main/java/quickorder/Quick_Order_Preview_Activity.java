package quickorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;

import adapter.Quikorder_Cart_Preview_Adapter;
import adapter.Quikorder_Preview_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import address.distributoraddress.Select_DistributorActivity;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class Quick_Order_Preview_Activity extends AppCompatActivity {

  private Button Placeorder_btn;
  private ProgressDialog progressDialog;
  private TextView your_Details_text, dealer_Name_text, dealer_Mobile_text, dealer_Address_text,
      tittle;
  private TextView dealer_Name, dealer_Mobile, dealer_Address, partnumber_desc_textview,
      quantity_textview, price_textview, edit_delete_textview;
  private ArrayList<String> PartNumList, DescriptionList, QuantityList;
  private ArrayList<Integer> PartIDList, Pricelist;
  private String description;
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private CartDAO cartDAO;
  private SQLiteDatabase db;
  private DBHelper dbHelper;
  private ListView listview;
  private Alertbox alert;
  private String selectedLanguage,from;
  private ImageView back;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_quick_order_preview);

    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();
    from = getIntent().getStringExtra("from");
    cartDAO = new CartDAO(Quick_Order_Preview_Activity.this);

    your_Details_text = (TextView) findViewById(R.id.your_Details);
    dealer_Name_text = (TextView) findViewById(R.id.dealer_Name);
    dealer_Mobile_text = (TextView) findViewById(R.id.dealer_Mobile);
    dealer_Address_text = (TextView) findViewById(R.id.dealer_Address);
    tittle = (TextView) findViewById(R.id.tittle);

    progressDialog = new ProgressDialog(Quick_Order_Preview_Activity.this);

    PartNumList = new ArrayList<String>();
    DescriptionList = new ArrayList<String>();
    QuantityList = new ArrayList<String>();
    PartIDList = new ArrayList<Integer>();
    Pricelist = new ArrayList<Integer>();
    listview = (ListView) findViewById(R.id.listview);

    // Alertbox alert = new Alertbox(Quick_Order_Preview_Activity.class);

    dealer_Name = (TextView) findViewById(R.id.dealer_name);
    dealer_Mobile = (TextView) findViewById(R.id.dealer_mobile);
    dealer_Address = (TextView) findViewById(R.id.dealer_address);

    partnumber_desc_textview = (TextView) findViewById(R.id.partnumber_desc_textview);
    quantity_textview = (TextView) findViewById(R.id.quantity_textview);
    price_textview = (TextView) findViewById(R.id.price_textview);
    edit_delete_textview = (TextView) findViewById(R.id.edit_delete_textview);

    Placeorder_btn = (Button) findViewById(R.id.preview);

    dbHelper = new DBHelper(Quick_Order_Preview_Activity.this);
    alert = new Alertbox(Quick_Order_Preview_Activity.this);
    dealer_Name.setText(myshare.getString("shopname", ""));
    dealer_Mobile.setText(myshare.getString("phone", ""));
    dealer_Address.setText(myshare.getString("address", ""));
    selectedLanguage = myshare.getString("language", "English");

    Placeorder_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        /*if (dealer_Address.getText().length() == 0) {
          final AlertDialog.Builder alertDialog =
                  new AlertDialog.Builder(Quick_Order_Preview_Activity.this);
          alertDialog.setTitle("Wabco");
          alertDialog.setMessage(R.string.delivery_address_alert);
          alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              startActivity(new Intent(Quick_Order_Preview_Activity.this, EditProfileActivity.class).putExtra("from","quickorder"));
            }
          });
          alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
          });
          alertDialog.show();
        } else {
          startActivity(
                  new Intent(Quick_Order_Preview_Activity.this, Select_DistributorActivity.class)
                          .putExtra("from", from));
        }*/

        startActivity(
                new Intent(Quick_Order_Preview_Activity.this, Select_DistributorActivity.class)
                        .putExtra("from", from));

        }
/*gfdgdf
* Quick_Order_Preview_Activity
* if (dealer_Address.getText().length() == 0) {
          final AlertDialog.Builder alertDialog =
              new AlertDialog.Builder(Quick_Order_Activity.this);
          alertDialog.setTitle("Wabco");
          alertDialog.setMessage(R.string.delivery_address_alert);
          alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              startActivity(new Intent(Quick_Order_Activity.this, EditProfileActivity.class).putExtra("from","quickorder"));
            }
          });
          alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
          });
          alertDialog.show();
        } else {
*
*
* */



    });
    if(from.equals("CartItem"))
    {
      GetPreviewDetails();
    }else{
      new GetPreviewDetails().execute();
    }


    this.back = (ImageView) findViewById(R.id.back);
    this.back.setOnClickListener(new View.OnClickListener() {


      public void onClick(View arg0) {
        onBackPressed();
      }
    });
    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(Quick_Order_Preview_Activity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Quick_Order_Preview_Activity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Quick_Order_Preview_Activity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });




  }
  void GetPreviewDetails(){
    CartDAO cartDAO = new CartDAO(Quick_Order_Preview_Activity.this);
    CartDTO cartDTO = cartDAO.GetCartItems();
    Quikorder_Cart_Preview_Adapter Adapter =
            new Quikorder_Cart_Preview_Adapter(Quick_Order_Preview_Activity.this, cartDTO );
    listview.setAdapter(Adapter);

  }
  private class GetPreviewDetails extends AsyncTask<String, Void, String> {

    Cursor cursor;

    protected void onPreExecute() {
      super.onPreExecute();
      progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      progressDialog.setCancelable(false);
      progressDialog.setMessage("Loading...");
      progressDialog.show();
      db = dbHelper.readDataBase();

    }

    @Override
    protected String doInBackground(String... params) {
      // TODO Auto-generated method stub
      Log.v("OFF Line", "SQLITE Table");
      try {
        String query = "select * from QuickOrder";
        Log.v("Dealer Name", query);
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
          do {
            PartIDList.add(cursor.getInt(cursor.getColumnIndex("partid")));
            PartNumList.add(cursor.getString(cursor.getColumnIndex("partnumber")));
            DescriptionList.add(cursor.getString(cursor.getColumnIndex("description")));
            QuantityList.add(cursor.getString(cursor.getColumnIndex("quantity")));
            Pricelist.add(cursor.getInt(cursor.getColumnIndex("price")));

          } while (cursor.moveToNext());
          cursor.close();
          db.close();
          return "received";
        } else {
          cursor.close();
          db.close();
          return "nodata";
        }
      } catch (Exception e) {
        cursor.close();
        db.close();
        Log.v("Error in Incentive", e.getMessage());
        return "notsuccess";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      progressDialog.dismiss();

      switch (result) {
        case "received":
          Quikorder_Preview_Adapter Adapter =
              new Quikorder_Preview_Adapter(Quick_Order_Preview_Activity.this, PartIDList,
                  PartNumList, DescriptionList, QuantityList, Pricelist);
          listview.setAdapter(Adapter);
          break;
        case "nodata":

          final AlertDialog.Builder alertDialog =
              new AlertDialog.Builder(Quick_Order_Preview_Activity.this);
          alertDialog.setTitle("WABCO");
          alertDialog.setMessage(R.string.order_not_found);
          alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              finish();
            }
          });

          alertDialog.show();

          break;
        default:

          break;
      }

    }

  }



}


