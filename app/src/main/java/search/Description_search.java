package search;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import productfamily.ProductFamilyItemAdapter;
import quickorder.Quick_Order_Preview_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class Description_search extends Activity {
    private ListView AssListView;
    private ArrayAdapter<String> DataAdapter;
    public List<String> Description;
    public List<String> Flag_List;
    public String INPUT_DB_PATH;
    public List<String> Image_List;
    String Item="";
    public List<String> PartNoList;
    public String ProductName;
    private FloatingActionButton arrleft,arrright;
    private HorizontalScrollView assemply;
    private ImageView backImageView;
    private LinearLayout flagtext;
    private MaterialSpinner from;
    private Button go;
    public String item;
    public ProgressDialog loadDialog;
    public List<String> partListList;
    private TextView partNo;
    public ProductFamilyItemAdapter productFamilyItemAdapter;
    private String search_word;
    private ListView serListView;
    private HorizontalScrollView service;
    public VehicleServicePartAdapter serviceadapter;
    TextView serviceorrepair;
    private TextView title;
    List<String> type;
    private String typeofserch="";
    public List<Bitmap> vehicleImage;
    public List<String> vehicleNameList2;
    EditText word;
    private Bitmap bitmap;
    private ArrayList<Bitmap> productbitmap;
    public List<String> ProductNameList;
    public List<String> ProductIdList;
    public List<String> vehicleIDList, productIDList;
    List<String>  priceflagList;
    Alertbox alert = new Alertbox(Description_search.this);
    String fromstring;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;
    private ImageView Cart_Icon;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_search);
        word = (EditText) findViewById(R.id.editText1);
        from = (MaterialSpinner) findViewById(R.id.spinner1);
        assemply = (HorizontalScrollView) findViewById(R.id.horilist);
        service = (HorizontalScrollView) findViewById(R.id.horilist2);
        AssListView = (ListView) findViewById(R.id.listView);
        serListView = (ListView) findViewById(R.id.vehicle_listView);
        serviceorrepair = (TextView) findViewById(R.id.serviceorrepair);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        flagtext = (LinearLayout) findViewById(R.id.flagtxt);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        from.setBackgroundResource(R.drawable.autotextback);


        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        productbitmap = new ArrayList<Bitmap>();
        go = (Button) findViewById(R.id.go2);
        type = new ArrayList<String>();
        vehicleImage = new ArrayList<Bitmap>();
        productIDList = new ArrayList<String>();



        type.add("Select Type");
        type.add("Assembly Part");
        type.add("Service Part");
        type.add("Repair Part");


        from.setItems(type);
        from.setPadding(30, 0, 0, 0);



        from.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                if (!item.toString().equals("Select Type")) {
                    Item = item.toString();
                    typeofserch = item.toString();

                    Log.v("Selected Item", Item.toString());
                    Log.v("Selected Item", typeofserch.toString());

                }
            }

        });




        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
                            .putExtra("from", "CartItem"));
                }
                // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });


        go.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (Item == null || Item.equals("Select Type") || Item.equals("")) {
                    Toast.makeText(Description_search.this, "Please Select  Search Type", Toast.LENGTH_LONG).show();
                } else if (typeofserch.equals("Assembly Part")) {
                    assemply.setVisibility(View.VISIBLE);
                    service.setVisibility(View.GONE);
                    search_word = word.getText().toString();
                    if (search_word.length() < 3 || search_word == null) {
                        word.setError("Minimum 3 letters required");
                    } else {
                        Log.v("Assemply ", "search");
                        new RetriveAssemplyearch().execute();
                    }
                } else if (typeofserch.equals("Service Part")) {
                    service.setVisibility(View.VISIBLE);
                    assemply.setVisibility(View.GONE);
                    search_word = word.getText().toString();
                    serviceorrepair.setText("Service Part-No");
                    item = "Service Part-No";
                    if (search_word.length() < 3 || search_word == null) {
                        word.setError("Minimum 3 letters required");
                    } else {
                        new Retrivesevicesearch().execute();
                    }
                } else if (typeofserch.equals("Repair Part")) {
                    service.setVisibility(View.VISIBLE);
                    assemply.setVisibility(View.GONE);
                    serviceorrepair.setText("Repair Part");
                    search_word = word.getText().toString();
                    item = "Repair Part No";
                    if (search_word.length() < 3 || search_word == null) {
                        word.setError("Minimum 3 letters required");
                    } else {
                        new Retrivesevicesearch().execute();
                    }
                }
            }
        });
        arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (Item.equals("Assembly Part")||Item.equals("")) {
                    assemply.smoothScrollTo(assemply.getScrollX() + 2000, assemply.getScrollY());
                } else {
                    service.smoothScrollTo(service.getScrollX() + 1000, service.getScrollY());
                    Log.v("in service", "yeah");
                }
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (Item.equals("Assembly Part")||Item.equals("")) {
                    assemply.smoothScrollTo(assemply.getScrollX() - 2000, assemply.getScrollY());
                } else {
                    Log.v("in service", "yeah");
                    service.smoothScrollTo(service.getScrollX() - 1000, service.getScrollY());
                }
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });

        backImageView = (ImageView) findViewById(R.id.back);

        backImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Description_search.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Description_search.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Description_search.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Description_search.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Description_search.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Description_search.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Description_search.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Description_search.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Description_search.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Description_search.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Description_search.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetriveAssemplyearch extends AsyncTask<Void, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(Description_search.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            Log.v("in descrription ", "Assemply searchpart");
            loadDialog.show();
        }

        protected String doInBackground(Void... params) {
            SearchDAO searchDAO = new SearchDAO(Description_search.this);
            SearchDTO searchDTO = new SearchDTO();
            if (search_word.isEmpty()) {
                return "notxt";
            }
            searchDTO = searchDAO.retriveForDescription(search_word);
            vehicleNameList2 = searchDTO.getVehicleName();
            Description = searchDTO.getDescription();
            partListList = searchDTO.getPartno_List();
            Image_List = searchDTO.getService_List();
            ProductName = searchDTO.getProductNameList();
            Flag_List = searchDTO.getFlag();
            vehicleIDList = searchDTO.getVehicleID();
            productIDList = searchDTO.getProductIDList();

            if (vehicleNameList2.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                flagtext.setVisibility(View.GONE);
                alert.showAlertbox("No Assembly parts Found !");
                if (productFamilyItemAdapter != null) {
                    productFamilyItemAdapter.clear();
                    AssListView.setAdapter(productFamilyItemAdapter);
                }
            }

		/*	for (int i = 0; i < Image_List.size(); i++) {

				productbitmap.add(downloadImage(Image_List.get(i)+".jpg"));


			}
*/
            productFamilyItemAdapter = new ProductFamilyItemAdapter(Description_search.this, vehicleIDList, vehicleNameList2, Description, partListList, ProductName, Flag_List, Image_List, productIDList);
            AssListView.setAdapter(productFamilyItemAdapter);
            arrright.setVisibility(View.VISIBLE);
            if (!Flag_List.isEmpty()) {
                flagtext.setVisibility(View.VISIBLE);
            }
        }
    }

    class Retrivesevicesearch extends AsyncTask<Void, String, String> {

        private List<String> vehicleIDList;
        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(Description_search.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
            Log.v("Calling ", "Service Repair kit searchpart");
        }

        protected String doInBackground(Void... arg0) {
            SearchDAO searchDAO = new SearchDAO(Description_search.this);
            SearchDTO searchDTO = new SearchDTO();
            searchDTO = searchDAO.retriveForServiceDescription(search_word, item);
            Description = searchDTO.getDescription();
            partListList = searchDTO.getPartno_List();
            PartNoList = searchDTO.getPartno_A();
            ProductIdList = searchDTO.getProductIDList();
            ProductNameList = searchDTO.getProductNameList2();
            vehicleNameList2 = searchDTO.getVehicleName();
            priceflagList = searchDTO.getPriceflaglist();
            vehicleIDList = searchDTO.getVehicleID();

            if (Description.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {

                String msg = "";
                if (item.equals("Service Part-No")) {
                    msg = "Service part are Not Available";
                    alert.showAlertbox(msg);
                } else {
                    msg = "Repair part are Not Applicable";
                    alert.showAlertbox(msg);
                }

                if (serviceadapter != null) {
                    serviceadapter.clear();
                    serListView.setAdapter(serviceadapter);
                }
            }
            serviceadapter = new VehicleServicePartAdapter(Description_search.this, partListList, Description, PartNoList, vehicleIDList, vehicleNameList2, ProductIdList, ProductNameList,priceflagList,fromstring);
            serListView.setAdapter(serviceadapter);
            arrright.setVisibility(View.VISIBLE);
        }
    }

    private Bitmap downloadImage(String url) {

        try {
            InputStream fileInputStream = getAssets().open(url);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 5;
            bitmap = BitmapFactory.decodeStream(fileInputStream, null, bmOptions);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Error", e.getMessage());
            return downloadImage("noimagefound");

        }
        return bitmap;
    }


    private void showAlert(String string) {
        // TODO Auto-generated method stub

        alert.showAlertbox(string);
    }


}
