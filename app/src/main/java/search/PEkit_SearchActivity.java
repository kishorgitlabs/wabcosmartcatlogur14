package search;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_KitAdapter;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import productfamily.ProductFamilyItemAdapter;
import quickorder.Quick_Order_Preview_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class PEkit_SearchActivity extends Activity {
    public List<String> Description;
    public List<String> Flag_List;
    public String INPUT_DB_PATH;
    public List<String> Image_List;
    public List<String> PartNoList;
    public String ProductName;
    public ArrayAdapter<String> adapter;
    ImageView arrleft;
    ImageView arrright;
    private ImageView backImageView;
    public List<String> descrip;
    private TextView flagtext;
    private Button go;
    HorizontalScrollView hsv;
    public String item;
    public ProgressDialog loadDialog;
    public List<String> partListList;
    private ListView partListListView;
    private TextView partNo;
    public List<String> partcodeno;
    public List<String> partnoList;
    public PE_KitAdapter pe_KitAdapter;
    private SharedPreferences preferences;
    public ProductFamilyItemAdapter productFamilyItemAdapter;
    private EditText textView;
    private TextView title;
    public List<Bitmap> vehicleImage;
    public List<String> vehicleNameList2;
	public List<String> flagList,priceflagList;

    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;
    private ImageView Cart_Icon;

    
    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pekit__search);
        this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        this.arrleft = (ImageView) findViewById(R.id.left);
        this.arrright = (ImageView) findViewById(R.id.right);
        this.textView = (EditText) findViewById(R.id.textato);
        this.partNo = (TextView) findViewById(R.id.textFontDesign1);
        this.title = (TextView) findViewById(R.id.textView2);
        this.vehicleImage = new ArrayList<Bitmap>();

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);


        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);

        this.arrright.setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
		});
        this.arrleft.setOnClickListener(new OnClickListener() {
			
        	 public void onClick(View v) {
                 hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                 arrleft.setVisibility(View.GONE);
                 arrright.setVisibility(View.VISIBLE);
             }
		});

        
        this.go = (Button) findViewById(R.id.go);
        
        this.go.setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
               try {
                   item = textView.getText().toString();
                   Log.v("on text Changed", item);
                   if (item.length() >= 3) {
                       new RetrieveFileAsynforselect().execute();
                   } else {
                       textView.setError("Minimum 3 Letters Please");
                   }
               }catch (Exception ex)
                {
                 ex.getMessage();
                }
            }
		});
        this.preferences = getSharedPreferences("DOWNLOAD_CHECK", 0);
        this.partListListView = (ListView) findViewById(R.id.partlist_listView);
        this.backImageView = (ImageView) findViewById(R.id.back);
        
        
        this.backImageView.setOnClickListener(new OnClickListener() {
			
        	public void onClick(View arg0) {
                onBackPressed();
            }
		});
        
        final ImageView menu = (ImageView)findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(PEkit_SearchActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(PEkit_SearchActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
						case R.id.search:
							startActivity(new Intent(PEkit_SearchActivity.this, SearchActivity.class));
							break;
						case R.id.notification:
							startActivity(new Intent(PEkit_SearchActivity.this, NotificationActivity.class));
							break;
						case R.id.vehicle:
							startActivity(new Intent(PEkit_SearchActivity.this, VehicleMakeActivity.class));
							break;
						case R.id.product:
							startActivity(new Intent(PEkit_SearchActivity.this, ProductFamilyActivity.class));
							break;
						case R.id.performance:
							startActivity(new Intent(PEkit_SearchActivity.this, PE_Kit_Activity.class));
							break;
                            case R.id.contact:
                                startActivity(new Intent(PEkit_SearchActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(PEkit_SearchActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(PEkit_SearchActivity.this, PriceListActivity.class));
                                break;
						case R.id.update:
							WabcoUpdate update = new WabcoUpdate(PEkit_SearchActivity.this);
							update.checkVersion();
							break;
						}
						return false;
					}
				});
				pop.setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(PopupMenu arg0) {
						// TODO Auto-generated method stub
						pop.dismiss();
					}
				});

				pop.inflate(R.menu.main);
				pop.show();
			}
		});


        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
                            .putExtra("from", "CartItem"));
                }
                // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });


    }


    class RetrieveFileAsynforselect extends AsyncTask<Void, Void, String> {

            protected void onPreExecute () {
            super.onPreExecute();
            loadDialog = new ProgressDialog(PEkit_SearchActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            SearchDAO searchDAO = new SearchDAO(PEkit_SearchActivity.this);
            SearchDTO searchDTO = new SearchDTO();
            if (item.isEmpty()) {
                return "notxt";
            }
            searchDTO = searchDAO.retriveForPEkit(item);
            partnoList = searchDTO.getpartnoList();
            partcodeno = searchDTO.getpartcodeno();
            descrip = searchDTO.getdescrip();
            flagList = searchDTO.getFlag();
            priceflagList = searchDTO.getPriceflaglist();
            if (partnoList.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                showAlert("No data found !");
                if (pe_KitAdapter != null) {
                    pe_KitAdapter.clear();
                    partListListView.setAdapter(pe_KitAdapter);
                }
            } else if (result.equals("notxt")) {
                pe_KitAdapter.clear();
                partListListView.setAdapter(pe_KitAdapter);
            } else {
                pe_KitAdapter = new PE_KitAdapter(PEkit_SearchActivity.this, partnoList, partcodeno, descrip, flagList, priceflagList);
                partListListView.setAdapter(pe_KitAdapter);
                arrright.setVisibility(View.VISIBLE);
            }
        }


    }




    private void showAlert(String string) {
		// TODO Auto-generated method stub
		Alertbox alert = new Alertbox(PEkit_SearchActivity.this);
		alert.showAlertbox(string);
	}


    
}
