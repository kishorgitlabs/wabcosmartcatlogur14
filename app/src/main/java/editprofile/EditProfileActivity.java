package editprofile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import dealer.account.Dealer_Account_Activity;
import distributor.account.Distributor_Account_Activity;
import models.usermodel.User;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfileActivity extends AppCompatActivity {

  private Button update, cancel;
  private View heade_Layout;
  private TextView Tittle;
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;

  private EditText name;
  private EditText phone;
  private EditText email, country, city,pincode, usertype, shopname, address,dealercode,gstnumber,pannumber;
  private MaterialSpinner state;
  private String[] drawerResourcesList;
  private List<String> stateList;
  private String idstring, namestring, phonestring, emailstring, citystring, countrystring,pincodestring,
      currentDate, SelectedState, android_id, ShopNameString, AddressString, userstring;
  private Alertbox box = new Alertbox(EditProfileActivity.this);
  private LinearLayout AddressLayout;
  private ImageView account,cart_icon,back;
  private String ComingFrom;
  private int SelectedStatePosition,SelectedUserPosition;
  private Matcher matcher;
  private Pattern pattern;
  private static final String GST_PATTERN =
          "\\d{2}[A-Z]{5}\\d{4}[A-Z]{1}\\d[Z]{1}[A-Z\\d]{1}";
  private static final String PAN_PATTERN = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  private String deviceId;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_profile);

    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();

    ComingFrom = getIntent().getStringExtra("from");


    name = (EditText) findViewById(R.id.nameedit);
    phone = (EditText) findViewById(R.id.phoneedit);
    email = (EditText) findViewById(R.id.emailedit);
    state = (MaterialSpinner) findViewById(R.id.stateedit);
    city = (EditText) findViewById(R.id.cityedit);
    pincode = (EditText) findViewById(R.id.pincodeedit);
    country = (EditText) findViewById(R.id.countryedit);
    usertype = (EditText) findViewById(R.id.usertypeedit);
    shopname = (EditText) findViewById(R.id.shopedit);
    address = (EditText) findViewById(R.id.addressedit);
    AddressLayout = (LinearLayout) findViewById(R.id.address_layout);
    gstnumber = (EditText) findViewById(R.id.gstedit);
    dealercode = (EditText) findViewById(R.id.delaercode_edit);
    pannumber = (EditText) findViewById(R.id.panedit);
    update = (Button) findViewById(R.id.okay);
    cancel = (Button) findViewById(R.id.cancel);

    state.setBackgroundResource(R.drawable.autotextback);

    heade_Layout = findViewById(R.id.header_layout);
    Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
    account = (ImageView) heade_Layout.findViewById(R.id.account_icon);
    cart_icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
    back = (ImageView) heade_Layout.findViewById(R.id.back);
    Tittle.setText(R.string.edit_profile);


    name.setText(myshare.getString("name", ""));
    phone.setText(myshare.getString("phone", ""));
    email.setText(myshare.getString("email", ""));
    city.setText(myshare.getString("city", ""));
    pincode.setText(myshare.getString("pincode", ""));
    country.setText(myshare.getString("country", ""));
    usertype.setText(myshare.getString("usertype", ""));

    pannumber.setText(myshare.getString("pannumber", ""));
    gstnumber.setText(myshare.getString("gstnumber", ""));
    shopname.setText(myshare.getString("shopname", ""));
    address.setText(myshare.getString("address", ""));
    dealercode.setText(myshare.getString("dealercode", ""));

    deviceId = myshare.getString("mobileid", "");
    if (myshare.getString("usertype", "").equals("Dealer")
        || myshare.getString("usertype", "").equals("OEM Dealer")) {
      shopname.setText(myshare.getString("shopname", ""));
      address.setText(myshare.getString("address", ""));
      AddressLayout.setVisibility(View.VISIBLE);
      cart_icon.setVisibility(View.GONE);
    } else {
      AddressLayout.setVisibility(View.GONE);
      cart_icon.setVisibility(View.GONE);
    }



    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    currentDate = df.format(c.getTime());



    drawerResourcesList = getResources().getStringArray(R.array.state_array);
    stateList = new ArrayList<String>();
    stateList = Arrays.asList(drawerResourcesList);

    usertype.setFocusable(false);
    state.setItems(stateList);
    state.setPadding(30, 0, 0, 0);

    state.setSelectedIndex(myshare.getInt("stateposition",0));

    state.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        SelectedState = item.toString();
        Log.v("State", SelectedState);
        SelectedStatePosition = position;
      }
    });


    account.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (myshare.getString("usertype", "").equals("Dealer")|| myshare.getString("name", "").equals("OEM Dealer")) {
          startActivity(new Intent(EditProfileActivity.this, Dealer_Account_Activity.class));
        } else if(myshare.getString("usertype", "").equals("Whole Sale Distributor")) {
          startActivity(new Intent(EditProfileActivity.this, Distributor_Account_Activity.class));
        }

      }
    });


    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });


    update.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // TODO Auto-generated method stub
         if (name.getText().toString().equals("")) {
          name.setError("Enter your Name");
          StyleableToast st =
                  new StyleableToast(EditProfileActivity.this, "Enter your Name !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();

        } else if (phone.getText().toString().length() != 10) {
          phone.setError("Phone number is not valid");
          StyleableToast st = new StyleableToast(EditProfileActivity.this,
                  "Phone number is not valid !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (state.getText().toString().equals("Select State")) {
          StyleableToast st =
                  new StyleableToast(EditProfileActivity.this, "Select your state !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (city.getText().toString().equals("")) {
          city.setError("Enter your City");
          StyleableToast st =
                  new StyleableToast(EditProfileActivity.this, "Enter your City !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (pincode.getText().toString().equals("")) {
          pincode.setError("Enter your Pincode");
          StyleableToast st =
                  new StyleableToast(EditProfileActivity.this, "Enter your Pincode !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (email.getText().length() != 0 && !isValidEmail(email.getText().toString().trim())) {
          email.setError("Email id is not valid");
          StyleableToast st = new StyleableToast(EditProfileActivity.this,
                  "Email id is not valid !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if (pannumber.getText().length() != 0 && !validatePanCard(pannumber.getText().toString().trim())) {
          pannumber.setError("PAN number is not valid");
          StyleableToast st = new StyleableToast(EditProfileActivity.this,
                  "PAN number is not valid !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else if ((usertype.getText().toString().equals("Dealer") || usertype.getText().toString().equals("OEM Dealer"))
                && (gstnumber.getText().toString().equals("") && dealercode.getText().toString().equals(""))) {
          box.showAlertbox(getString(R.string.gst_validation_error));

        } else {
          if(gstnumber.getText().toString().length() != 0  && !validateGst(gstnumber.getText().toString())) {
            gstnumber.setError("GST number is not valid");
            StyleableToast st = new StyleableToast(EditProfileActivity.this,
                    "GST number is not valid !", Toast.LENGTH_SHORT);
            st.setBackgroundColor(EditProfileActivity.this.getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
          }else {
            CheckInterNet();
          }
        }
      }


    });

    cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        onBackPressed();
      }
    });

  }





  public final static boolean isValidEmail(CharSequence target) {
    if (target == null) {
      return false;
    } else {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
  }

  public boolean validateGst(final String gstnumber) {
    pattern = Pattern.compile(GST_PATTERN);
    matcher = pattern.matcher(gstnumber);
    return matcher.matches();

  }


  public boolean validatePanCard(final String gstnumber) {

    pattern = Pattern.compile(PAN_PATTERN);
    matcher = pattern.matcher(gstnumber);
    return matcher.matches();

  }


  private void CheckInterNet() {

    NetworkConnection networkConnection = new NetworkConnection(EditProfileActivity.this);
    if (networkConnection.CheckInternet()) {
      UpdateUserDetails();
    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }
  }

  private void UpdateUserDetails() {

    final ProgressDialog progressDialog = ProgressDialog.show(EditProfileActivity.this,
        "Updating details", "please wait...", false, false);
    idstring = myshare.getString("id", "noid");
    namestring = name.getText().toString();
    phonestring = phone.getText().toString();
    emailstring = email.getText().toString();
    citystring = city.getText().toString();
    pincodestring = pincode.getText().toString();
    countrystring = country.getText().toString();
    userstring = usertype.getText().toString();
    ShopNameString = shopname.getText().toString();
    AddressString = address.getText().toString();
    SelectedState = state.getText().toString();



    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();
    APIService api = retrofit.create(APIService.class);
    Call<User> userapi = api.UpdateUser(idstring, namestring, emailstring, phonestring, userstring,
        citystring,pincodestring, SelectedState, countrystring, currentDate, deviceId, ShopNameString,
        AddressString, myshare.getString("username", ""), myshare.getString("password", ""), dealercode.getText().toString(),gstnumber.getText().toString(),
            pannumber.getText().toString());
    userapi.enqueue(new Callback<User>() {
      @Override
      public void onResponse(Call<User> call, Response<User> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
          Log.v("result", response.body().getResult().toString());
          if (response.body().getResult().toString().equals("Updated")) {
            OnSuccess();
          } else if(response.body().getResult().toString().equals("Registered")){
            OnSuccess();
          }
          else
          {
            box.showAlertbox(getResources().getString(R.string.server_error));
          }
        } else {
          box.showAlertbox(getResources().getString(R.string.server_error));
        }
      }

      @Override
      public void onFailure(Call<User> call, Throwable t) {
        progressDialog.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.slow_internet_connection));
      }
    });
  }



  private void OnSuccess() {

    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
    alertDialog.setMessage("Your details has been updated successfully !");
    alertDialog.setTitle("WABCO");
    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        edit.putString("name", namestring);
        edit.putString("email", emailstring);
        edit.putString("phone", phonestring);
        edit.putString("country", countrystring);
        edit.putString("state", SelectedState);
        edit.putInt("stateposition", SelectedStatePosition);
        edit.putString("city", citystring);
        edit.putString("pincode", pincodestring);
        edit.putString("shopname", ShopNameString);
        edit.putString("address", AddressString);
        edit.putString("gstnumber",gstnumber.getText().toString());
        edit.putString("pannumber",pannumber.getText().toString());
        edit.commit();
        recreate();
        dialog.dismiss();

          onBackPressed();
        }


    });
    // alertDialog.setIcon(R.drawable.logo);
    alertDialog.show();


  }



}
