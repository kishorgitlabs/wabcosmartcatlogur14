package contact;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.wabco.brainmagic.wabco.catalogue.EquivalentActivity;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.List;

import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceList;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;



public class ContactActivity extends Activity {
    List<String> Address;
    private ListView AddressListView;
    List<String> CityList;
    public List<String> CityName;
    public ArrayAdapter<String> DataAdapter;
    public ArrayAdapter<String> DataAdapter2;
    public ArrayAdapter<String> DataAdapter3;
    List<String> Distributor;
    List<String> Email1;
    List<String> Email2;
    List<String> Email3;
    List<String> StateList;
    public List<String> StateName;
    private String Statename;
    ImageView arrleft;
    ImageView arrright;
    protected String cityname;
    private MaterialSpinner cityspinner;
    ContactAdapter conadap;

    private String cotacttype;
    HorizontalScrollView hsv;
    List<String> phone;
    public ProgressDialog progressDialog;
    private MaterialSpinner statespinner;
    List<String> typeList;
    private ImageView vehicleBackImageView;
    private ArrayList<String> ZoneList, CityListgrid;
    private RelativeLayout zonelayout;
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private List<String> contact_type;
    private Button gobtn;
    private String contact_typefrom;
    private TextView typetxt, titletxt;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        this.StateName = new ArrayList<String>();
        this.CityName = new ArrayList<String>();
        this.CityList = new ArrayList<String>();
        this.typeList = new ArrayList<String>();
        this.StateList = new ArrayList<String>();
        this.ZoneList = new ArrayList<String>();
        this.Distributor = new ArrayList<String>();
        this.Address = new ArrayList<String>();
        this.phone = new ArrayList<String>();
        this.Email1 = new ArrayList<String>();
        this.Email2 = new ArrayList<String>();
        this.Email3 = new ArrayList<String>();
        this.CityListgrid = new ArrayList<String>();
        this.statespinner = (MaterialSpinner) findViewById(R.id.spinstate);
        this.cityspinner = (MaterialSpinner) findViewById(R.id.spincty);
        this.AddressListView = (ListView) findViewById(R.id.listView);
        this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        this.arrleft = (ImageView) findViewById(R.id.left);
        this.arrright = (ImageView) findViewById(R.id.right);
        typetxt = (TextView) findViewById(R.id.ctypetxt);

        titletxt = (TextView) findViewById(R.id.tittle);

        //	this.typeList.add(0, "Select Contact Type");

        contact_typefrom = getIntent().getStringExtra("contacttype").toString();
        typetxt.setText(getIntent().getStringExtra("name").toString() + " Name");

        titletxt.setText(getIntent().getStringExtra("contacttype").toString());

        this.typeList.add("All");
        this.typeList.add("Authorised Service Centre");
        this.typeList.add("Field staff");
        this.typeList.add("Distributor");

		/*ZoneList.add("Select Zone");
        ZoneList.add("North");
		ZoneList.add("South");
		ZoneList.add("East");
		ZoneList.add("West");*/

        gobtn = (Button) findViewById(R.id.gocontact);
        new RetriveStatesForSpinner().execute();
        hsv.setVisibility(View.GONE);


        this.vehicleBackImageView = (ImageView) findViewById(R.id.back);
        this.vehicleBackImageView.setOnClickListener(new OnClickListener() {


            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        this.arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 2000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });

        this.arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 2000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });


        this.cityspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                cityname = item.toString();

            }

        });


        statespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                Statename = item.toString();
                if (!Statename.equals("Select State")) {
                    new RetriveCityNameForSipnner().execute(new String[0]);
                }

            }
        });


        gobtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statespinner.getText().toString().equals("Select State")) {
                    Toast.makeText(ContactActivity.this, "Select Sate", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated method stub
                    if (cityspinner.getText().toString().equals("All")) {
                        String query = "select distinct * from address where   State_Name='" + Statename + "'  and contact_type ='" + contact_typefrom + "' ";
                        new RetriveDealerAddress().execute(query);
                    } else {
                        String query = "select distinct * from address where  City ='" + cityspinner.getText().toString() + "' and State_Name='" + statespinner.getText().toString() + "' and contact_type ='" + contact_typefrom + "'  ";
                        new RetriveDealerAddress().execute(query);
                    }
                }

            }
        });


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(ContactActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(ContactActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(ContactActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(ContactActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(ContactActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(ContactActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(ContactActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(ContactActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(ContactActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(ContactActivity.this, PriceList.class));
                                break;
                            case R.id.equivalent:
                                startActivity(new Intent(ContactActivity.this, EquivalentActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(ContactActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }

    public class RetriveCityNameForSipnner extends AsyncTask<String, Void, String> {

        private ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            loading = ProgressDialog.show(ContactActivity.this,"WABCO","Loading...",false,false);

        }

        protected String doInBackground(String... params) {
            ContactDAO contactDAO = new ContactDAO(ContactActivity.this);
            ContactDTO contactDTO = new ContactDTO();
            contactDTO = contactDAO.retriveCity(Statename, getIntent().getStringExtra("contacttype").toString());
            CityName = contactDTO.getCityName();
            Log.v("Clicked state in city", Statename);
            if (CityName.isEmpty()) {
                return "unsuccess";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loading.dismiss();
            if (result.equals("success")) {
                DataAdapter2 = new ArrayAdapter<String>(ContactActivity.this, R.layout.simple_spinner_item, CityName);
                CityName.add(0, "All");

                cityspinner.setItems(CityName);
                cityspinner.setPadding(30, 0, 0, 0);

            }

        }
    }

    class RetriveDealerAddress extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ContactActivity.this);
            progressDialog.setProgressStyle(0);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading...");
            //progressDialog.show();
        }

        protected String doInBackground(String... params) {
            ContactDAO contactDAO = new ContactDAO(ContactActivity.this);
            ContactDTO contactDTO = new ContactDTO();

            Log.v("Query +++  ", params[0]);

            contactDTO = contactDAO.retriveAddress(params[0]);
            StateName = contactDTO.getStateName();
            CityList = contactDTO.getCityName();
            Distributor = contactDTO.getDistributorName();
            Address = contactDTO.getAddress();
            phone = contactDTO.getphone();
            Email1 = contactDTO.getEmail1();
            Email2 = contactDTO.getEmail2();
            Email3 = contactDTO.getEmail3();
            contact_type = contactDTO.getContactType();
            if (StateName.isEmpty()) {
                return "unsuccess";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                progressDialog.dismiss();
                conadap = new ContactAdapter(ContactActivity.this, StateName, CityList, Distributor, Address, phone, Email1, Email2, Email3, contact_type);
                AddressListView.setAdapter(conadap);
                arrright.setVisibility(View.VISIBLE);
                hsv.setVisibility(View.VISIBLE);


            } else {
                hsv.setVisibility(View.GONE);
                showAlert("No ASCData Found !");

            }
        }
    }




    class RetriveStatesForSpinner extends AsyncTask<Void, Void, String> {


        private ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            loading = ProgressDialog.show(ContactActivity.this,"WABCO","Loading...",false,false);

        }

        protected String doInBackground(Void... params) {
            ContactDAO contactDAO = new ContactDAO(ContactActivity.this);
            ContactDTO contactDTO = new ContactDTO();
            contactDTO = contactDAO.retriveState(getIntent().getStringExtra("contacttype").toString());
            StateName = contactDTO.getStateName();
            if (StateName.isEmpty()) {
                return "unsuccess";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loading.dismiss();
            if (result.equals("success")) {
                DataAdapter = new ArrayAdapter<String>(ContactActivity.this, R.layout.simple_spinner_item, StateName);
                StateName.add(0, "Select State");

                statespinner.setItems(StateName);
                statespinner.setPadding(30, 0, 0, 0);

            }

        }
    }

    private void showAlert(String string) {
        // TODO Auto-generated method stub
        //conadap.clear();
        Alertbox box = new Alertbox(ContactActivity.this);
        box.showAlertbox(string);
    }


}
