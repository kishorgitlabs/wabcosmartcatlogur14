package contact;

import java.util.ArrayList;
import java.util.List;

public class ContactDTO {
    List<String> Email1;
    List<String> Email2;
    List<String> Email3;
    private List<String> address;
    private List<String> cityList;
    private List<String> distributor;
    private List<String> phone;
    private List<String> stateList;
	private List<String> contact_type;
    private List<String> name;
    private List<Boolean> faveList;
    private List<String> distributor_code;
    public ContactDTO()
    {
        this.Email1 = new ArrayList<String>();
        this.Email2 = new ArrayList<String>();
        this.Email3 = new ArrayList<String>();
    }

    public void setStateNameList(List<String> stateList) {
        this.stateList = stateList;
    }

    public List<String> getStateName() {
        return this.stateList;
    }

    public void setCityNameList(List<String> cityList) {
        this.cityList = cityList;
    }

    public List<String> getCityName() {
        return this.cityList;
    }

    public void setDistributorList(List<String> distributor) {
        this.distributor = distributor;
    }

    public void setAddressList(List<String> address) {
        this.address = address;
    }

    public void setphoneList(List<String> phone) {
        this.phone = phone;
    }

    public void setEmail1List(List<String> email1) {
        this.Email1 = email1;
    }

    public void setEmail2List(List<String> email2) {
        this.Email2 = email2;
    }

    public void setEmail3List(List<String> email3) {
        this.Email3 = email3;
    }

    public List<String> getDistributorName() {
        return this.distributor;
    }

    public List<String> getAddress() {
        return this.address;
    }

    public List<String> getphone() {
        return this.phone;
    }

    public List<String> getEmail1() {
        return this.Email1;
    }

    public List<String> getEmail2() {
        return this.Email2;
    }

    public void setFaveList(List<Boolean> faveList) {
        this.faveList = faveList;
    }

    public List<String> getEmail3() {
        return this.Email3;
    }

	public void setContacttype(List<String> contact_type) {
		// TODO Auto-generated method stub
		this.contact_type=contact_type;

	}

	public List<String> getContactType() {
		// TODO Auto-generated method stub
		return contact_type;
	}

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<Boolean> getFaveList() {
        return faveList;
    }

    public List<String> getDistributor_code() {
        return distributor_code;
    }

    public void setDistributor_code(List<String> distributor_code) {
        this.distributor_code = distributor_code;
    }
}
