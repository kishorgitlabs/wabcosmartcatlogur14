package notification;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import models.whatsnew.WhatsData;

@SuppressLint({"InflateParams"})
public class NotifiAdapter extends RecyclerView.Adapter<NotifiAdapter.ViewHolder> {
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    Context con;
    List<String> date;
    List<String> descrip;
    public File dir;
    String ffname;
    public File filepath;
    List<String> msg;
    public ProgressDialog progressDialog;
    public String url;
    private List<WhatsData> whatsnewdataadapter;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameof;
       // public TextView sno;
        public TextView txtdes;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            this.nameof = (TextView) itemLayoutView.findViewById(R.id.editdat1);
            this.txtdes = (TextView) itemLayoutView.findViewById(R.id.textView2);
            //this.sno = (TextView) itemLayoutView.findViewById(R.id.textView1);
        }
    }

    public NotifiAdapter(Context con,  List<WhatsData> whatsnewdataadapter) {
        this.msg = new ArrayList<String>();
        this.descrip = new ArrayList<String>();
        this.date = new ArrayList<String>();
        this.whatsnewdataadapter=whatsnewdataadapter;
        this.con = con;
    }


	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_msg, null));
        Log.v("inside myadap msgs", "viewhol");
        return viewHolder;
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.nameof.setText((CharSequence) whatsnewdataadapter.get(position).getNotificationname());
       // viewHolder.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        viewHolder.txtdes.setText((CharSequence) whatsnewdataadapter.get(position).getDiscription());
    }

  

    public int getItemCount() {
        return whatsnewdataadapter.size();
    }
}
