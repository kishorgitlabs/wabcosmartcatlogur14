package addcart;

import java.util.List;

/**
 * Created by system01 on 6/13/2017.
 */

public class CartDTO  {
    private List<String> partIDList;
    private List<String> partNoList;
    private List<String> partdescriptionList;
    private List<String> partNameList;
    private List<String> partTypeList;
    private List<String> partQuantityList;
    private List<String> partPriceList;
    private List<String> partImageList;
    private List<String> partCodeList;


    public List<String> getPartIDList() {
        return partIDList;
    }

    public void setPartIDList(List<String> partIDList) {
        this.partIDList = partIDList;
    }

    public List<String> getPartNoList() {
        return partNoList;
    }

    public void setPartNoList(List<String> partNoList) {
        this.partNoList = partNoList;
    }

    public List<String> getPartdescriptionList() {
        return partdescriptionList;
    }

    public void setPartdescriptionList(List<String> partdescriptionList) {
        this.partdescriptionList = partdescriptionList;
    }

    public List<String> getPartNameList() {
        return partNameList;
    }

    public void setPartNameList(List<String> partNameList) {
        this.partNameList = partNameList;
    }

    public List<String> getPartTypeList() {
        return partTypeList;
    }

    public void setPartTypeList(List<String> partTypeList) {
        this.partTypeList = partTypeList;
    }

    public List<String> getPartQuantityList() {
        return partQuantityList;
    }

    public void setPartQuantityList(List<String> partQuantityList) {
        this.partQuantityList = partQuantityList;
    }

    public List<String> getPartPriceList() {
        return partPriceList;
    }

    public void setPartPriceList(List<String> partPriceList) {
        this.partPriceList = partPriceList;
    }

    public List<String> getPartImageList() {
        return partImageList;
    }

    public void setPartImageList(List<String> partImageList) {
        this.partImageList = partImageList;
    }

    public List<String> getPartCodeList() {
        return partCodeList;
    }

    public void setPartCodeList(List<String> partCodeList) {
        this.partCodeList = partCodeList;
    }
}
